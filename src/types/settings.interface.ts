export interface SettingsInterface {
  address: string;
  email: string;
  phone: string;
  tgLink: string;
  ytLink: string;
  wsLink: string;
}
