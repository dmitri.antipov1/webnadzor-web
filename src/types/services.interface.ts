export interface ServicesInterface {
  id: number;

  category: 'accreditation' | 'license' | 'site';

  name: string;

  price: string;

  createdAt: Date;

  updatedAt: Date;
}
