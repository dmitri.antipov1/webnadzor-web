export interface ReviewsInterface {
  id: number;
  author: string;
  organization: string;
  review: string;
  isVisible: boolean;
  createdAt: string;
  updatedAt: string;
}
