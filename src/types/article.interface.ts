import { UserInterface } from './user.interface';

export interface ArticleInterface {
  id: number;

  slug: string;

  title: string;

  description: string;

  body: string;

  category: string;

  createdAt: Date;

  updatedAt: Date;

  author: UserInterface;
}
