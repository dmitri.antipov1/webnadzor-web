export interface EmailRequestInterface {
  name: string;
  email: string;
  phone: string;
  message: string;
}
