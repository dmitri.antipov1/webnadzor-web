export interface UserInterface {
  bio: string;
  email: string;
  id: number
  image: string;
  position: string;
  token: string;
  username: string;
}
