import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { delay, finalize, Observable } from 'rxjs';
import { LoaderService } from '../services/loader.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  timer: any;
  constructor(public loaderService: LoaderService) { }

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(this.timer) {
      clearTimeout(this.timer);
    }

    this.timer = setTimeout(() => this.loaderService.toggle(true), 0)

    // made a small delay for the correct display of the spinner
    return next.handle(req).pipe(
      delay(500),
      finalize(() => this.loaderService.toggle(false)),
    );
  }
}
