import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of, switchMap } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.auth.isAuth$.pipe(
      switchMap((authenticated) => {
        if (authenticated) {
          return of(true);
        }
        this.router.navigate(['/']);
        return of(false);
      })
    );
  }

}
