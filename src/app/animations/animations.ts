import { animate, state, style, transition, trigger } from '@angular/animations';

export const inOutPaneAnimation = trigger("inOutPaneAnimation", [
  transition(":enter", [
    style({ opacity: 0, transform: "translateX(100%)" }), //apply default styles before animation starts
    animate(
      "500ms ease-in-out",
      style({ opacity: 1, transform: "translateX(0)" }),
    ),
  ]),
  transition(":leave", [
    style({ opacity: 1, transform: "translateX(0)" }), //apply default styles before animation starts
    animate(
      "500ms ease-in-out",
      style({ opacity: 0, transform: "translateX(100%)" }),
    ),
  ]),
]);

export const fade = trigger('fade', [
  state('void', style({ opacity: 0 })),
  state('*', style({ opacity: 1 })),
  transition(':enter', animate('1000ms ease-out')),
  transition(':leave', animate('1000ms ease-in')),
]);
