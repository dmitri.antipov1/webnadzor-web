import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { BehaviorSubject, switchMap } from 'rxjs';
import { ArticleInterface } from '../../types/article.interface';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  articles$ = new BehaviorSubject<ArticleInterface[]>([]);
  article$ = new BehaviorSubject<ArticleInterface | null>(null);

  constructor(private apiService: ApiService) { }

  getAllArticles(): void {
    this.apiService.getAllArticlesFromApi().subscribe((res) => this.articles$.next(res));
  }

  getSingleArticles(slug: string): void {
    this.apiService.getSingleArticlesFromApi(slug).subscribe((res) => this.article$.next(res));
  }

  createArticle(body: ArticleInterface): void {
    const modifiedBody = {article: body}
    this.apiService.addArticle(modifiedBody).pipe(
      switchMap(() => {
        return this.apiService.getAllArticlesFromApi();
      })
    ).subscribe((res) => this.articles$.next(res));
  }

  public removeArticle(articleSlug: string): void {
    this.apiService.removeArticle(articleSlug)
      .pipe(
        switchMap(() => {
          return this.apiService.getAllArticlesFromApi();
        })
      ).subscribe((res) => this.articles$.next(res));
  }

  public updateArticle(body: ArticleInterface, slug: string): void {
    this.apiService.updateSingleArticlesFromApi(body, slug)
      .pipe(switchMap((res) => {
        return this.apiService.getSingleArticlesFromApi(slug)
      }))
      .subscribe((res) => this.article$.next(res));
  }
}
