import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  toggle(state: boolean): void {
    this.isLoading$.next(state);
  }
}
