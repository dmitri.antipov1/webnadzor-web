import { Injectable } from '@angular/core';
import { LoginInterface } from '../../types/login.interface';
import { BehaviorSubject } from 'rxjs';
import { UserInterface } from '../../types/user.interface';
import { Router } from '@angular/router';
import { ApiService } from './api/api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$ = new BehaviorSubject<UserInterface | null>(null);
  isAuth$ = new BehaviorSubject<boolean>(this.isTokenValid());

  constructor(
    private apiService: ApiService,
    private router: Router,
  ) {}

  login(loginData: LoginInterface): void {
    this.apiService.loginUser(loginData).subscribe({
      next: (user) => {
        this.user$.next(user);
        this.isAuth$.next(true);
        this.setAccessToken(user.token);
        this.router.navigate(['admin/news']);
      },
      error: () => {
        this.router.navigate(['/']);
      }
    });
  }

  logout(): void {
    localStorage.clear();
    this.isAuth$.next(false);
    this.router.navigate(['/']);
  }

  private setAccessToken(token: string): void {
    localStorage.setItem('at', token);
  }

  public getAccessToken(): string | null {
    return localStorage.getItem('at');
  }

  private isTokenValid(): boolean {
    const token = this.getAccessToken();
    if (token !== null) {
      return true;
    }
    return false;
  }
}
