import { Injectable } from '@angular/core';
import { BehaviorSubject, delay, Observable, switchMap } from 'rxjs';
import { SettingsInterface } from '../../types/settings.interface';
import { ApiService } from './api/api.service';
import { ServicesInterface } from '../../types/services.interface';

@Injectable({
  providedIn: 'root',
})
export class ServicesService {
  public settings$ = new BehaviorSubject<SettingsInterface | null>(null)
  public allServices$ = new BehaviorSubject<ServicesInterface[]>([]);
  public singleService$ = new BehaviorSubject<ServicesInterface | null>(null);
  public categoryService$ = new BehaviorSubject<ServicesInterface[] | null>(null);

  constructor(private apiService: ApiService) {
  }

  public getSettings(): void {
    this.apiService.getSettingsFromApi().subscribe((settings) => this.settings$.next(settings));
  }

  public updateSettings(value: SettingsInterface) {
    this.apiService.updateSettingsFromApi(value)
      .pipe(delay(500)).subscribe((res) => {
      this.settings$.next(res)
    })
  }

  public addServices(services: ServicesInterface) {
    this.apiService.addServices(services).pipe(
      switchMap(() => {
        return this.apiService.getAllServices()
      }),
    ).subscribe((res) => this.allServices$.next(res));
  }

  public getAllServices() {
    this.apiService.getAllServices().subscribe((res) => this.allServices$.next(res));
  }

  public removeService(serviceId: number) {
    this.apiService.deleteServices(serviceId)
      .pipe(switchMap(() => {
        return this.apiService.getAllServices();
      })).subscribe((res) => this.allServices$.next(res));
  }

  public getSingleService(id: number): void {
    this.apiService.getSingleService(id).subscribe((res) => this.singleService$.next(res));
  }

  public updateService(serviceId: number, value: ServicesInterface): void {
    this.apiService.updateServices(serviceId, value).subscribe((res) => {
        this.singleService$.next(res);
    });
  }

  public getServicesCategory(category: string): void {
    this.apiService.getSingleCategoryServices(category).subscribe((res) => this.categoryService$.next(res));
  }
}
