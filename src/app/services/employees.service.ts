import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { BehaviorSubject, switchMap } from 'rxjs';
import { EmployeeInterface } from '../../types/employee.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  employees$ = new BehaviorSubject<EmployeeInterface[]>([]);
  oneEmployee$ = new BehaviorSubject<EmployeeInterface | null>(null);

  constructor(private as: ApiService) {}

  getAllEmployees(): void {
    this.as.getAllEmployees().subscribe((res) => this.employees$.next(res));
  }

  getSingleEmployee(id: number): void {
    this.as.getSingleEmployee(id).subscribe((res) => this.oneEmployee$.next(res));
  }

  createEmployee(employeeBody: FormData): void {
    this.as.addEmployee(employeeBody).pipe(
      switchMap(() => this.as.getAllEmployees())
    ).subscribe((res) => this.employees$.next(res))
  }

  updateEmployee(id: number, employeeBody: EmployeeInterface): void {
    this.as.updateEmployee(id, employeeBody).pipe(
      switchMap(() => this.as.getSingleEmployee(id))
    ).subscribe((res) => this.oneEmployee$.next(res))
  }

  removeEmployee(id: number): void {
    this.as.deleteEmployee(id).pipe(
      switchMap(() => this.as.getAllEmployees())
    ).subscribe((res) => this.employees$.next(res))
  }
}
