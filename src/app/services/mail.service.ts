import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { EmailRequestInterface } from '../../types/emailRequest.interface';
import { BehaviorSubject } from 'rxjs';
import { CalculatorInterface } from '../../types/calculator.interface';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  success$ = new BehaviorSubject<boolean>(false);
  error$ = new BehaviorSubject<boolean>(false);

  timer: any;

  constructor(private apiService: ApiService) { }

  sendFormToEmail(mailBody: EmailRequestInterface): void {
    this.apiService.sendEmail(mailBody).subscribe((res) => {
        this.success$.next(true);
        if (this.timer) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => this.success$.next(false), 3000);
    }, () => {
      this.error$.next(true);
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => this.error$.next(false), 3000);
    })
  }

  public sendEgiszForm(value: CalculatorInterface) {
    this.apiService.sendCalculatedEmail(value).subscribe((res) => {
      this.success$.next(true);
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => this.success$.next(false), 3000);
    }, () => {
      this.error$.next(true);
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => this.error$.next(false), 3000);
    })
  }
}
