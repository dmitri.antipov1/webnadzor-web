import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { LoginInterface } from '../../../types/login.interface';
import { map, Observable } from 'rxjs';
import { UserInterface } from '../../../types/user.interface';
import { SettingsInterface } from '../../../types/settings.interface';
import { ArticleInterface } from '../../../types/article.interface';
import { ReviewsInterface } from '../../../types/reviews.interface';
import { ServicesInterface } from '../../../types/services.interface';
import { EmployeeInterface } from '../../../types/employee.interface';
import { EmailRequestInterface } from '../../../types/emailRequest.interface';
import { CalculatorInterface } from '../../../types/calculator.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) { }

  // User

  public loginUser(data: LoginInterface): Observable<UserInterface> {
    const newDataUser = {user: data}
    return this.http.post<{user: UserInterface}>(`${this.url}/users/login`, newDataUser)
      .pipe(map((res) => res.user))
  }

  // Settings

  public getSettingsFromApi(): Observable<SettingsInterface> {
    return this.http
      .get<{settings: SettingsInterface}>(`${this.url}/settings`).pipe(map((res) => res.settings));
  }

  public updateSettingsFromApi(body: SettingsInterface): Observable<SettingsInterface> {
    return this.http
      .put<{settings: SettingsInterface}>(`${this.url}/settings/1`, body).pipe(map((res) => res.settings));
  }

  public sendEmail(mailBody: EmailRequestInterface): Observable<any> {
    return this.http.post<any>(`${this.url}/email`, mailBody)
  }

  public sendCalculatedEmail(value: CalculatorInterface): Observable<any> {
    return this.http.post<any>(`${this.url}/email/calculator`, value)
  }

  // Articles

  public getAllArticlesFromApi(): Observable<ArticleInterface[]> {
    return this.http
      .get<ArticleInterface[]>(`${this.url}/articles`);
  }

  public addArticle(body: {article: ArticleInterface}): Observable<ArticleInterface> {
    return this.http
      .post<ArticleInterface>(`${this.url}/articles`, body);
  }

  public getSingleArticlesFromApi(slug: string): Observable<ArticleInterface> {
    return this.http
      .get<{article: ArticleInterface}>(`${this.url}/articles/${slug}`).pipe(map((res) => res.article));
  }

  public removeArticle(articleSlug: string): Observable<ArticleInterface> {
    return this.http
      .delete<ArticleInterface>(`${this.url}/articles/${articleSlug}`);
  }

  public updateSingleArticlesFromApi(body: ArticleInterface, slug: string): Observable<ArticleInterface> {
    const requestBody = {article: body};
    return this.http.put<ArticleInterface>(`${this.url}/articles/${slug}`, requestBody)
  }

  // Reviews

  public getAllReviews(): Observable<ReviewsInterface[]> {
    return this.http
      .get<ReviewsInterface[]>(`${this.url}/reviews`);
  }

  public getSingleReview(id: number): Observable<ReviewsInterface> {
    return this.http
      .get<ReviewsInterface>(`${this.url}/reviews/${id}`);
  }

  public getEnabledReviews(): Observable<ReviewsInterface[]> {
    return this.http
      .get<ReviewsInterface[]>(`${this.url}/reviews/enabled`);
  }

  public removeReview(reviewId: number): Observable<ReviewsInterface> {
    return this.http
      .delete<ReviewsInterface>(`${this.url}/reviews/${reviewId}`);
  }

  public editReview(id: number, editedReview: ReviewsInterface): Observable<ReviewsInterface> {
    return this.http
      .put<ReviewsInterface>(`${this.url}/reviews/${id}`, editedReview);
  }

  public addReview(value: ReviewsInterface): Observable<ReviewsInterface> {
    return this.http
      .post<ReviewsInterface>(`${this.url}/reviews/create`, value);
  }

  // Services
  public getAllServices(): Observable<ServicesInterface[]> {
    return this.http
      .get<{services: ServicesInterface[]}>(`${this.url}/services`).pipe(map((res) => res.services));;
  }

  public addServices(services: ServicesInterface): Observable<ServicesInterface> {
    return this.http
      .post<ServicesInterface>(`${this.url}/services`, services);
  }

  public deleteServices(servicesId: number): Observable<ServicesInterface> {
    return this.http
      .delete<ServicesInterface>(`${this.url}/services/${servicesId}`);
  }

  public updateServices(id: number, editedService: ServicesInterface): Observable<ServicesInterface> {
    return this.http
      .put<ServicesInterface>(`${this.url}/services/${id}`, editedService);
  }

  public getSingleService(id: number): Observable<ServicesInterface> {
    return this.http
      .get<ServicesInterface>(`${this.url}/services/${id}`);
  }

  public getSingleCategoryServices(category: string): Observable<ServicesInterface[]> {
    return this.http.get<{services: ServicesInterface[]}>(`${this.url}/services/${category}`).pipe(map((res) => res.services))
  }

  // Employees

  public getAllEmployees(): Observable<EmployeeInterface[]> {
    return this.http
      .get<EmployeeInterface[]>(`${this.url}/employees`);
  }

  public addEmployee(employee: FormData): Observable<EmployeeInterface> {
    return this.http
      .post<EmployeeInterface>(`${this.url}/employees`, employee);
  }

  public deleteEmployee(employeeId: number): Observable<EmployeeInterface> {
    return this.http
      .delete<EmployeeInterface>(`${this.url}/employees/${employeeId}`);
  }

  public updateEmployee(id: number, employee: EmployeeInterface): Observable<EmployeeInterface> {
    return this.http
      .put<EmployeeInterface>(`${this.url}/employees/${id}`, employee);
  }

  public getSingleEmployee(id: number): Observable<EmployeeInterface> {
    return this.http
      .get<EmployeeInterface>(`${this.url}/employees/${id}`);
  }
}
