import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { BehaviorSubject, switchMap } from 'rxjs';
import { ReviewsInterface } from '../../types/reviews.interface';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  public reviews$: BehaviorSubject<ReviewsInterface[]> = new BehaviorSubject<ReviewsInterface[]>([]);
  public availableReviews$: BehaviorSubject<ReviewsInterface[]> = new BehaviorSubject<ReviewsInterface[]>([]);
  public review$: BehaviorSubject<ReviewsInterface | null> = new BehaviorSubject<ReviewsInterface | null>(null);
  public backMessage$ = new BehaviorSubject<string | null>(null);

  constructor(private as: ApiService) { }

  getAllReviews(): void {
    this.as.getAllReviews().subscribe((res) => this.reviews$.next(res));
  }

  public removeReview(reviewId: number): void {
    this.as.removeReview(reviewId)
      .pipe(switchMap(() => {
        return this.as.getAllReviews();
      }))
      .subscribe((res) => this.reviews$.next(res));
  }

  public getSingleReview(id: number): void {
    this.as.getSingleReview(id).subscribe((res) => this.review$.next(res));
  }

  public editReview(review: ReviewsInterface): void {
    const {id, isVisible} = review;
    const editedReview = {...review, isVisible: !isVisible}
    this.as.editReview(id, editedReview)
      .pipe(switchMap(() => {
        return this.as.getAllReviews();
      }))
      .subscribe()
  }

  public getAvailableReviews() {
    this.as.getEnabledReviews().subscribe((res) => this.availableReviews$.next(res))
  }

  public addReview(value: ReviewsInterface) {
    this.as.addReview(value).subscribe({
      next: () => {
        this.backMessage$.next('Спасибо за Ваш отзыв!')
      },
      error: () => {
        this.backMessage$.next('Приносим извенения, на данный момент эта услуга недоступна')
      },
    })
  }
}
