import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { EmployeesService } from '../../../services/employees.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-employees-page',
  templateUrl: './admin-employees-page.component.html',
  styleUrls: ['./admin-employees-page.component.css']
})
export class AdminEmployeesPageComponent implements OnInit {
  employees$ = this.es.employees$;
  isRemovePopup: boolean;
  employeeId: number;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef | undefined;
  public isFormVisible: boolean = false;
  private timer: any;
  public employeeForm: FormGroup;
  public file: File;

  constructor(
    private es: EmployeesService,
    private fb: FormBuilder
  ) {
    this.employeeForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      position: ['', Validators.required],
      image: ['', Validators.required],
      bio: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.es.getAllEmployees();
  }

  public openRemoveModal(id: number): void {
    this.isRemovePopup = true;
    this.employeeId = id;
  }

  public removeEmployee() {
    this.es.removeEmployee(this.employeeId);
    this.isRemovePopup = false;
  }

  toggleFormState(): void {
    this.isFormVisible = true;
    if(this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      if(this.myScrollContainer?.nativeElement) {
        this.myScrollContainer?.nativeElement.scrollIntoView({ behavior: 'smooth'});
      }
    }, 500);
  }

  public async addEmployee() {
    if (this.employeeForm.valid) {
      const fd = {...this.employeeForm.value, image: await this.toBase64(this.file)}
      this.es.createEmployee(fd);
      this.employeeForm.reset();
    }
  }


  public onFileSelected($event: any) {
    this.file = $event.target.files[0];
  }

  private toBase64(file: File): Promise<unknown> {
    return  new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

}
