import { AfterContentInit, Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../../../../services/employees.service';
import { EmployeeInterface } from '../../../../../types/employee.interface';

@Component({
  selector: 'app-admin-employee-edit',
  templateUrl: './admin-employee-edit.component.html',
  styleUrls: ['./admin-employee-edit.component.css'],
})
export class AdminEmployeeEditComponent implements OnInit, OnDestroy, AfterContentInit {
  private subscription = new Subscription();
  public employee$ = this.es.oneEmployee$;
  public employeeForm: FormGroup;
  public employeeId: number;
  private file: File;
  public isOpenBox = false;
  public error$ = new BehaviorSubject(false);

  constructor(
    private route: ActivatedRoute,
    private es: EmployeesService,
    private fb: FormBuilder,
  ) {
    this.employeeForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', Validators.required],
      position: ['', Validators.required],
      bio: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.subscription = this.route.params
      .subscribe((res) => {
        const { id } = res;
        this.employeeId = id;
        this.es.getSingleEmployee(id);
      });
  }

  public ngAfterContentInit() {
    this.subscription = this.employee$.subscribe(async (res) => {
      this.employeeForm.patchValue({
        username: res?.username ? res?.username : '',
        email: res?.email ? res?.email : '',
        position: res?.position ? res?.position : '',
        bio: res?.bio ? res?.bio : '',
      })
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.employeeForm.reset();
  }

  public async editEmployee() {
    if (this.employeeForm.valid) {
      const fb = {} as EmployeeInterface;
      if (this.file) {
        Object.assign(fb, {...this.employeeForm.value, image: await this.toBase64(this.file)})
      } else {
        Object.assign(fb, {...this.employeeForm.value, image: this.employee$.value?.image})
      }
      this.es.updateEmployee(this.employeeId, fb);
    }
  }

  public changePhoto($event: any) {
      const photo = $event.target.files[0];
      if(photo.size > 5000000) {
        this.error$.next(true);
        return;
      } else {
        this.file = photo;
        this.error$.next(false);
      }
  }

  public openEditImageBox() {
    this.isOpenBox = !this.isOpenBox;
    this.employeeForm.patchValue({
      image: this.employee$.value?.image
    })
  }

  private toBase64(file: File): Promise<unknown> {
    return  new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
}
