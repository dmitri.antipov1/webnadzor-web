import { AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ServicesService } from '../../../services/services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-settings-page',
  templateUrl: './admin-settings-page.component.html',
  styleUrls: ['./admin-settings-page.component.css']
})
export class AdminSettingsPageComponent implements OnInit, OnDestroy {
  settings$ = this.ss.settings$;
  public settingsForm: FormGroup;
  subscription = new Subscription();

  constructor(
    private ss: ServicesService,
    private fb: FormBuilder
  ) {
    this.settingsForm = this.fb.group({
      phone: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      ytLink: ['', Validators.required],
      tgLink: ['', Validators.required],
      wsLink: ['', Validators.required],
    })
  }

  public ngOnInit() {
    this.subscription = this.settings$.subscribe((res) => {
      this.settingsForm.patchValue({
        phone: res?.phone,
        email: res?.email,
        address: res?.address,
        ytLink: res?.ytLink,
        tgLink: res?.tgLink,
        wsLink: res?.wsLink,
      })
    })
  }


  public save(): void {
    if (this.settingsForm.valid) {
      this.ss.updateSettings(this.settingsForm.value);
    }
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
