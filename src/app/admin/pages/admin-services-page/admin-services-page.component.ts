import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-admin-services-page',
  templateUrl: './admin-services-page.component.html',
  styleUrls: ['./admin-services-page.component.css']
})
export class AdminServicesPageComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef | undefined;
  public isFormVisible: boolean = false;
  private timer: any;
  public servicesForm: FormGroup;
  public services$ = this.ss.allServices$;
  public isRemovePopup: boolean = false;
  public serviceId: number;

  constructor(
    private fb: FormBuilder,
    private ss: ServicesService
  ) {
    this.servicesForm = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      category: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.ss.getAllServices();
  }

  toggleFormState(): void {
    this.isFormVisible = true;
    if(this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      if(this.myScrollContainer?.nativeElement) {
        this.myScrollContainer?.nativeElement.scrollIntoView({ behavior: 'smooth'});
      }
    }, 500)
  }

  public save(): void {
    if (this.servicesForm.valid) {
      this.ss.addServices(this.servicesForm.value);
      this.servicesForm.reset();
    }
  }

  public openModal(id: number) {
    this.isRemovePopup = true;
    this.serviceId = id;
  }

  public removeService(): void {
    this.ss.removeService(this.serviceId);
    this.isRemovePopup = false;
  }
}
