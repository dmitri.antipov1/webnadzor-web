import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from '../../../../services/services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-services-edit',
  templateUrl: './admin-services-edit.component.html',
  styleUrls: ['./admin-services-edit.component.css']
})
export class AdminServicesEditComponent implements OnInit, AfterViewInit, OnDestroy {
  public servicesForm: FormGroup;
  public serviceId: number;
  public service$ = this.ss.singleService$;
  private subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private ss: ServicesService,
    private route: ActivatedRoute,
  ) {
    this.servicesForm = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      category: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.params
      .subscribe((res) => {
      const { id } = res;
      this.serviceId = +id;
    })
    this.ss.getSingleService(this.serviceId);
  }

  public ngAfterViewInit(): void {
    this.subscription = this.service$.subscribe((res) => {
      this.servicesForm.patchValue({
        name: res?.name,
        price: res?.price,
        category: res?.category
      })
    })
  }

  public editForm() {
    if (this.servicesForm.valid) {
      this.ss.updateService(this.serviceId, this.servicesForm.value);
    }
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
