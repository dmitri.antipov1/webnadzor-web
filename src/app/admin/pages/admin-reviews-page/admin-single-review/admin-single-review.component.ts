import { Component, OnInit } from '@angular/core';
import { ReviewsService } from '../../../../services/reviews.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-single-review',
  templateUrl: './admin-single-review.component.html',
  styleUrls: ['./admin-single-review.component.css']
})
export class AdminSingleReviewComponent implements OnInit {
  review$ = this.rs.review$;

  constructor(
    private route: ActivatedRoute,
    private rs: ReviewsService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((res) => {
      const { id } = res;
      this.rs.getSingleReview(+id)
    })
  }

}
