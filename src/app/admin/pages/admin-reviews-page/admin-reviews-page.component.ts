import { Component, OnInit } from '@angular/core';
import { ReviewsService } from '../../../services/reviews.service';
import { ReviewsInterface } from '../../../../types/reviews.interface';

@Component({
  selector: 'app-admin-reviews-page',
  templateUrl: './admin-reviews-page.component.html',
  styleUrls: ['./admin-reviews-page.component.css']
})
export class AdminReviewsPageComponent implements OnInit {
  reviews$ = this.rs.reviews$;
  isRemovePopup: boolean;
  reviewId: number;

  constructor(private rs: ReviewsService) { }

  ngOnInit(): void {
    this.rs.getAllReviews();
  }

  public openRemoveModal(id: number): void {
    this.isRemovePopup = true;
    this.reviewId = id;
  }

  public removeReview() {
    this.rs.removeReview(this.reviewId);
    this.isRemovePopup = false;
  }

  public editReview(review: ReviewsInterface): void {
    this.rs.editReview(review);
  }
}
