import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ArticlesService } from '../../../services/articles.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-news-page',
  templateUrl: './admin-news-page.component.html',
  styleUrls: ['./admin-news-page.component.css']
})
export class AdminNewsPageComponent implements OnInit {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef | undefined;
  public isFormVisible: boolean = false;
  private timer: any;
  public articleForm: FormGroup;
  public articles$ = this.as.articles$;
  public isRemovePopup = false;
  private articleId: string;

  constructor(private as: ArticlesService, private fb: FormBuilder) {
    this.articleForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
      body: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.as.getAllArticles();
  }

  toggleFormState(): void {
    this.isFormVisible = true;
    if(this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      if(this.myScrollContainer?.nativeElement) {
        this.myScrollContainer?.nativeElement.scrollIntoView({ behavior: 'smooth'});
      }
    }, 500);
  }

  addArticle(): void {
    if (this.articleForm.valid) {
      this.as.createArticle(this.articleForm.value);
      this.articleForm.reset();
    }
  }

  public openRemoveModal(slug: string): void {
    this.isRemovePopup = true;
    this.articleId = slug;
  }

  public removeArticle(): void {
    this.as.removeArticle(this.articleId);
    this.isRemovePopup = false;
  }
}
