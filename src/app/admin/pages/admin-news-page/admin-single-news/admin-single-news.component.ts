import { AfterContentInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ArticlesService } from '../../../../services/articles.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-single-news',
  templateUrl: './admin-single-news.component.html',
  styleUrls: ['./admin-single-news.component.css']
})
export class AdminSingleNewsComponent implements OnInit, AfterContentInit, OnDestroy {
  private subscription = new Subscription();
  public article$ = this.as.article$;
  public articleForm: FormGroup;
  public slug: string;

  constructor(
    private route: ActivatedRoute,
    private as: ArticlesService,
    private fb: FormBuilder,
  ) {
    this.articleForm = this.fb.group({
      title: [this.article$.value?.title, Validators.required],
      description: [this.article$.value?.description, Validators.required],
      category: [this.article$.value?.category, Validators.required],
      body: [this.article$.value?.body, Validators.required],
    })
  }

  ngOnInit(): void {
    this.subscription = this.route.params
      .subscribe((res) => {
        const {slug} = res;
        this.slug = slug;
        this.as.getSingleArticles(slug);
    });
  }

  public ngAfterContentInit() {
   this.subscription = this.article$.subscribe((res) => {
      this.articleForm.patchValue({
        title: res?.title,
        description: res?.description,
        category: res?.category,
        body: res?.body,
      })
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.articleForm.reset();
  }

  public editArticle() {
    if (this.articleForm.valid){
      this.as.updateArticle(this.articleForm.value, this.slug);
    }
  }
}
