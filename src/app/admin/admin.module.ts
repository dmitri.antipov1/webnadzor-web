import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { AdminNewsPageComponent } from './pages/admin-news-page/admin-news-page.component';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { AdminReviewsPageComponent } from './pages/admin-reviews-page/admin-reviews-page.component';
import { AdminServicesPageComponent } from './pages/admin-services-page/admin-services-page.component';
import { AdminEmployeesPageComponent } from './pages/admin-employees-page/admin-employees-page.component';
import { QuillModule } from 'ngx-quill';
import { AdminSettingsPageComponent } from './pages/admin-settings-page/admin-settings-page.component'
import { ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptor } from '../interceptors/token.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModalComponent } from './components/modal/modal.component';
import { AdminSingleNewsComponent } from './pages/admin-news-page/admin-single-news/admin-single-news.component';
import {
  AdminSingleReviewComponent,
} from './pages/admin-reviews-page/admin-single-review/admin-single-review.component';
import {
  AdminServicesEditComponent,
} from './pages/admin-services-page/admin-services-edit/admin-services-edit.component';
import { AdminEmployeeEditComponent } from './pages/admin-employees-page/admin-employee-edit/admin-employee-edit.component';

const routes: Routes = [
  {path: 'admin', component: AdminPageComponent, canActivate: [AuthGuard], children: [
      {path: 'news', component: AdminNewsPageComponent},
      {path: 'news/:slug', component: AdminSingleNewsComponent},
      {path: 'reviews', component: AdminReviewsPageComponent},
      {path: 'reviews/:id', component: AdminSingleReviewComponent},
      {path: 'services', component: AdminServicesPageComponent},
      {path: 'services/:id', component: AdminServicesEditComponent},
      {path: 'employees', component: AdminEmployeesPageComponent},
      {path: 'employees/:id', component: AdminEmployeeEditComponent},
      {path: 'settings', component: AdminSettingsPageComponent},
    ]},
]

@NgModule({
  declarations: [
    AdminNewsPageComponent,
    AdminPageComponent,
    AdminReviewsPageComponent,
    AdminServicesPageComponent,
    AdminEmployeesPageComponent,
    AdminSettingsPageComponent,
    ModalComponent,
    AdminSingleNewsComponent,
    AdminSingleReviewComponent,
    AdminServicesEditComponent,
    AdminEmployeeEditComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  exports: [
    ModalComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote'],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'color': ['#A47A5E', '#72897C', '#cecbcb', '#000000', '#F4F4F4', '#e54f4f'] }],          // dropdown with defaults from theme
          [{ 'align': [] }],

          ['link', 'video'],                         // link and image, video
        ],
      },
    }),
  ],
})
export class AdminModule { }
