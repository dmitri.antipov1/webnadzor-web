import { Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @ViewChild('modalContent') private modalContent: ElementRef;
  @Output() closeWindow = new EventEmitter()
  @Input() height: string = '150px';


  public clickOutside($event: MouseEvent) {
    if (this.modalContent && !this.modalContent.nativeElement.contains($event.target)) {
      this.closeWindow.emit();
    }
  }
}
