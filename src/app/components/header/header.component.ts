import { Component, EventEmitter, Input, Output } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('1000ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('1000ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})
export class HeaderComponent {
  @Input() isAuth: boolean | null = null;
  @Output() logoutEvent = new EventEmitter();
  public isOpen = false;
  innerWidth = window.innerWidth

  constructor(public router: Router) {}

  public logout(): void {
    this.logoutEvent.emit();
  }

  public close() {
    this.isOpen = false;
  }
}
