import { Component, Input, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { SettingsInterface } from '../../../types/settings.interface';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(100%)'}),
        animate('1000ms ease-in', style({transform: 'translateY(0)'}))
      ]),
      transition(':leave', [
        animate('1000ms ease-in', style({transform: 'translateY(100%)'}))
      ])
    ])
  ]
})
export class FooterComponent {
  @Input() settings: SettingsInterface | null = null;

  get isTeamAndMainPage(): boolean {
    return (this.router.url === '/' || this.router.url.includes('#')) || this.router.url.includes('team')
  };

  constructor(public router: Router) { }

}
