import { Component, OnInit } from '@angular/core';
import { animate, query, style, transition, trigger } from '@angular/animations';
import { AuthService } from './services/auth.service';
import { ServicesService } from './services/services.service';
import { SettingsInterface } from '../types/settings.interface';
import { LoaderService } from './services/loader.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('fadeInOutAnimation', [
      transition('* => *', [
        query(':enter', [style({ opacity: 0 })], {
          optional: true,
        }),
        query(
          ':leave',
          [
            style({ opacity: 1 }),
            animate('0.3s', style({ opacity: 0 })),
          ],
          { optional: true },
        ),
        query(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('0.3s', style({ opacity: 1 })),
          ],
          { optional: true },
        ),
      ]),
    ]),
  ],
})

export class AppComponent implements OnInit {
  isAuth$ = this.as.isAuth$;
  settings$ = this.ss.settings$;

  get isLoading$(): Observable<boolean> {
    return this.ls.isLoading$.asObservable();
  }

  constructor(
    private as: AuthService,
    private ss: ServicesService,
    private ls: LoaderService
  ) {}

  public ngOnInit(): void {
    this.ss.getSettings();
  }

  public logout() {
    this.as.logout();
  }
}
