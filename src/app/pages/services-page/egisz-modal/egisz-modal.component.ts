import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailService } from '../../../services/mail.service';

@Component({
  selector: 'app-egisz-modal',
  templateUrl: './egisz-modal.component.html',
  styleUrls: ['./egisz-modal.component.css'],
})
export class EgiszModalComponent {
  @ViewChild('modalContent') private modalContent: ElementRef | undefined;
  calculateForm: FormGroup;
  price = 0;
  timer: any;


  constructor(
    private router: Router,
    private fb: FormBuilder,
  ) {
    this.calculateForm = this.fb.group({
      isRegistration: ['5000', Validators.required],
      personal: ['1500', Validators.required],
      equipment: ['5000', Validators.required],
    })
  }

  calculate(): void {
    if (this.calculateForm.valid) {
      const personalPrice = parseInt(this.calculateForm.get('personal')?.value);
      const equipmentPrice = parseInt(this.calculateForm.get('equipment')?.value);
      const registrationPrice = parseInt(this.calculateForm.get('isRegistration')?.value);
      this.price = personalPrice + equipmentPrice + registrationPrice;
    }
  }

  send(): void {
    this.calculateForm.reset();
    if(this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.price = 0;
    }, 3000);
  }


  public navigateBack($event: MouseEvent) {
    if (this.modalContent && !this.modalContent?.nativeElement.contains($event.target)) {
      this.router.navigate(['/services']);
    }
  }

  public close() {
    this.router.navigate(['/services']);
  }
}
