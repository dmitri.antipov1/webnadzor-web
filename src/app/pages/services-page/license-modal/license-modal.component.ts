import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-license-modal',
  templateUrl: './license-modal.component.html',
  styleUrls: ['./license-modal.component.css']
})
export class LicenseModalComponent implements OnInit {
  @ViewChild('modalContent') private modalContent: ElementRef | undefined;
  public licenseService$ = this.ss.categoryService$;

  constructor(
    private router: Router,
    private ss: ServicesService,
  ) {
  }

  public ngOnInit(): void {
    this.ss.getServicesCategory('license');
  }

  public navigateBack($event: MouseEvent) {
    if (this.modalContent && !this.modalContent?.nativeElement.contains($event.target)) {
      this.router.navigate(['/services']);
    }
  }

  public close() {
    this.router.navigate(['/services']);
  }
}
