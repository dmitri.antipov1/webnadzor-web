import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-accreditation-modal',
  templateUrl: './accreditation-modal.component.html',
  styleUrls: ['./accreditation-modal.component.css']
})
export class AccreditationModalComponent implements OnInit {
  @ViewChild('modalContent') private modalContent: ElementRef | undefined;
  public accreditationService$ = this.ss.categoryService$;

  constructor(
    private router: Router,
    private ss: ServicesService,
  ) {
  }

  public ngOnInit(): void {
    this.ss.getServicesCategory('accreditation');
  }

  public navigateBack($event: MouseEvent) {
    if (this.modalContent && !this.modalContent?.nativeElement.contains($event.target)) {
      this.router.navigate(['/services']);
    }
  }

  public close() {
    this.router.navigate(['/services']);
  }
}
