import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-web-modal',
  templateUrl: './web-modal.component.html',
  styleUrls: ['./web-modal.component.css']
})
export class WebModalComponent implements OnInit {
  @ViewChild('modalContent') private modalContent: ElementRef | undefined;
  public siteService$ = this.ss.categoryService$;

  constructor(
    private router: Router,
    private ss: ServicesService
  ) {
  }

  public ngOnInit(): void {
    this.ss.getServicesCategory('site');
  }

  public navigateBack($event: MouseEvent) {
    if (this.modalContent && !this.modalContent?.nativeElement.contains($event.target)) {
      this.router.navigate(['/services']);
    }
  }

  public close() {
    this.router.navigate(['/services']);
  }
}
