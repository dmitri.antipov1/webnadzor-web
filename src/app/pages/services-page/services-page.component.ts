import { Component, OnInit } from '@angular/core';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class ServicesPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    document.body.style.backgroundImage = 'url(assets/services-2.png)'
  }

}
