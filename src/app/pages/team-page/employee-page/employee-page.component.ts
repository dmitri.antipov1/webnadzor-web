import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../../services/employees.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-page',
  templateUrl: './employee-page.component.html',
  styleUrls: ['./employee-page.component.css']
})
export class EmployeePageComponent implements OnInit {
  employee$ = this.es.oneEmployee$;
  employeeId: number;

  constructor(
    private es: EmployeesService,
    private route: ActivatedRoute)
  { }

  ngOnInit(): void {
    this.route.params.subscribe((res) => {
      const { id } = res;
      this.employeeId = id;
    });
    this.es.getSingleEmployee(this.employeeId);
    document.body.style.backgroundImage = 'url(assets/team-2-bg.png)'
  }

}
