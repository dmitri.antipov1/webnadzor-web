import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class TeamPageComponent implements OnInit {
  employees$ = this.es.employees$;

  constructor(private es: EmployeesService) { }

  ngOnInit(): void {
    this.es.getAllEmployees();
    document.body.style.backgroundImage = 'url(assets/team-2-bg.png)'
  }

}
