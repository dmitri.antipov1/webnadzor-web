import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { ArticlesService } from '../../../services/articles.service';
import { Subscription, switchMap } from 'rxjs';
import { fade, inOutPaneAnimation } from '../../../animations/animations';

@Component({
  selector: 'app-single-news',
  templateUrl: './single-news.component.html',
  styleUrls: ['./single-news.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class SingleNewsComponent implements OnInit, OnDestroy {
  public articleSlug: string | null = null;
  public article$ = this.articleService.article$;
  private subscription = new Subscription();

  constructor(
    private articleService: ArticlesService,
    private router: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.subscription = this.router.params
      .subscribe((res) => {
        const {slug} = res;
        this.articleSlug = slug;
      });

    if(this.articleSlug) {
      this.articleService.getSingleArticles(this.articleSlug);
    }

    document.body.style.backgroundImage = 'url(assets/news-bg.png)'
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
