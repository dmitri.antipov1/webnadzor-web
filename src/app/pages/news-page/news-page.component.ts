import { Component } from '@angular/core';
import { ArticlesService } from '../../services/articles.service';
import { BehaviorSubject } from 'rxjs';
import { ArticleInterface } from '../../../types/article.interface';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class NewsPageComponent {
  articles$: BehaviorSubject<ArticleInterface[]> = this.as.articles$;

  constructor(private as: ArticlesService) { }

  ngOnInit(): void {
    this.as.getAllArticles();
    document.body.style.backgroundImage = 'url(assets/news-bg.png)'
  }

}
