import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailService } from '../../services/mail.service';
import { Observable } from 'rxjs';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-contacts-page',
  templateUrl: './contacts-page.component.html',
  styleUrls: ['./contacts-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class ContactsPageComponent implements OnInit {
  settings$ = this.ss.settings$;
  public contactForm: FormGroup;

  constructor(
    private readonly ss: ServicesService,
    private fb: FormBuilder,
    private ms: MailService
  ) {
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: [''],
      message: ['', Validators.required],
      isAgree: ['', Validators.requiredTrue],
    })
  }

  get success$(): Observable<boolean> {
    return this.ms.success$.asObservable();
  }

  get error$(): Observable<boolean> {
    return this.ms.error$.asObservable();
  }

  ngOnInit(): void {
    document.body.style.backgroundImage = 'url(assets/contacts-2-bg.png)'
  }

  public send() {
    if (this.contactForm.valid) {
      this.ms.sendFormToEmail(this.contactForm.value);
      this.contactForm.reset();
    }
  }
}
