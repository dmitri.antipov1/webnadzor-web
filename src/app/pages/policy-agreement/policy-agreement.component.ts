import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-policy-agreement',
  templateUrl: './policy-agreement.component.html',
  styleUrls: ['./policy-agreement.component.css']
})
export class PolicyAgreementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    document.body.style.backgroundColor = 'var(--gray)'
  }

}
