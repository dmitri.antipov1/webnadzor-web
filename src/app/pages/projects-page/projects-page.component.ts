import { Component, OnInit } from '@angular/core';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class ProjectsPageComponent implements OnInit {
  public clients = [
    {img: 'client1.svg', url: 'https://марусяростов.рф/'},
    {img: 'client2.svg', url: 'https://dentalpalace.ru/'},
    {img: 'client3.svg', url: 'https://medmspb.ru/'},
    {img: 'client4.svg', url: 'https://rostov-rusmed.ru/'},
    {img: 'client5.svg', url: 'https://etalonkamensk.ru/'},
    {img: 'client6.svg', url: 'https://лазурь-бк.рф/'},
    {img: 'client7.svg', url: 'https://mrtshka.ru/'},
    {img: 'client8.svg', url: 'https://cosmetology-vdon.ru/'},
    {img: 'client9.svg', url: 'https://voxelpro.ru/'},
    {img: 'client10.svg', url: 'https://salus-clinic.com/'},
    {img: 'client11.svg', url: 'https://mrt-lider.ru/'},
    {img: 'client12.svg', url: 'https://tvoistom.ru/'},
    {img: 'client13.svg', url: 'http://allergia-rnd.ru/'},
    {img: 'client14.svg', url: 'https://blesk-tag.ru/'},
    {img: 'client15.svg', url: 'https://www.reformclinic.ru/'},
    {img: 'client16.svg', url: 'https://clinicbrukdon.ru/'},
    {img: 'client17.svg', url: 'https://plomba-ideal.ru/'},
    {img: 'client18.svg', url: 'https://dentigala.ru/main'},
    {img: 'client19.svg', url: 'http://ustomat.ru/'},
    {img: 'client20.svg', url: 'https://www.dentalleague.ru/'},
    {img: 'client21.svg', url: 'https://norddental.ru/?ysclid=le4e3mgdb174053752'},
    {img: 'client22.svg', url: 'https://tkz.su/'},
    {img: 'client23.svg', url: 'http://www.neyronn.ru/?ysclid=le61y34y8r475871302'},
    {img: 'client24.svg', url: 'http://cgbbataysk.ru/?ysclid=le4edvf06v40573501'},
    {img: 'client25.svg', url: 'https://slavia-rostov.ru/'},
    {img: 'client26.svg', url: 'https://lazurmedical.ru/?ysclid=le4e9yaq45179879413'},
    {img: 'client27.svg', url: 'https://bella-dent.ru/'},
    {img: 'client28.svg', url: 'https://beriev.com/?ysclid=le4dxlcr6u70636999'},
    {img: 'client29.svg', url: 'https://www.linline-clinic.ru/'},
  ]

  public ngOnInit() {
    document.body.style.backgroundImage = 'none';
  }

}
