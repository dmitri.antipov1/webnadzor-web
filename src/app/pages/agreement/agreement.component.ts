import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cookies-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.css']
})
export class AgreementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    document.body.style.backgroundColor = 'var(--gray)'
  }

}
