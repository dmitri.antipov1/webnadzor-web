import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { ComponentsModule } from '../components/components.module';
import { NewsPageComponent } from './news-page/news-page.component';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { RouterModule, Routes } from '@angular/router';
import { SingleNewsComponent } from './news-page/single-news/single-news.component';
import { ServicesPageComponent } from './services-page/services-page.component';
import { EgiszModalComponent } from './services-page/egisz-modal/egisz-modal.component';
import { AccreditationModalComponent } from './services-page/accreditation-modal/accreditation-modal.component';
import { LicenseModalComponent } from './services-page/license-modal/license-modal.component';
import { WebModalComponent } from './services-page/web-modal/web-modal.component';
import { ProjectsPageComponent } from './projects-page/projects-page.component';
import { TeamPageComponent } from './team-page/team-page.component';
import { EmployeePageComponent } from './team-page/employee-page/employee-page.component';
import { ContactsPageComponent } from './contacts-page/contacts-page.component';
import { ReviewsPageComponent } from './reviews-page/reviews-page.component';
import { PolicyAgreementComponent } from './policy-agreement/policy-agreement.component';
import { AgreementComponent } from './agreement/agreement.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminModule } from '../admin/admin.module';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  { path: 'news', component: NewsPageComponent },
  { path: 'news/:slug', component: SingleNewsComponent },
  { path: 'projects', component: ProjectsPageComponent },
  { path: 'contacts', component: ContactsPageComponent },
  { path: 'reviews', component: ReviewsPageComponent },
  { path: 'team', component: TeamPageComponent },
  { path: 'team/:id', component: EmployeePageComponent },
  { path: 'services', component: ServicesPageComponent },
  { path: 'services/egisz', component: EgiszModalComponent },
  { path: 'services/accreditation', component: AccreditationModalComponent },
  { path: 'services/license', component: LicenseModalComponent },
  { path: 'services/web', component: WebModalComponent },
  { path: 'agreement', component: AgreementComponent },
  { path: 'policy-agreement', component: PolicyAgreementComponent },
]

@NgModule({
  declarations: [
    MainPageComponent,
    NewsPageComponent,
    SingleNewsComponent,
    ServicesPageComponent,
    EgiszModalComponent,
    AccreditationModalComponent,
    LicenseModalComponent,
    WebModalComponent,
    ProjectsPageComponent,
    TeamPageComponent,
    EmployeePageComponent,
    ContactsPageComponent,
    ReviewsPageComponent,
    PolicyAgreementComponent,
    AgreementComponent,
    LoginPageComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    AngularFullpageModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    AdminModule,
  ],
})
export class PagesModule {
}
