import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailService } from '../../services/mail.service';
import { Observable } from 'rxjs';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade,
  ],
})
export class MainPageComponent implements OnInit {
  timer: any;
  config: any;
  infoIndex: number = 0;
  fullpage_api: any;
  isVisible = false;
  aboutInfo = [
    {
      id: 1,
      title: 'ЕГИСЗ',
      description: `<p>ЗАРЕГИСТРИРУЕМ ВАС В ЕГИСЗ В КРАТЧАЙШИЕ СРОКИ</p> <br> <p>СДЕЛАЕМ ПОЛНЫЙ АУДИТ ВАШЕЙ ДОКУМЕНТАЦИИ</p> <br> <p>ОБОРУДОВАНИЕ БУДЕТ ВНЕСЕНО В РЕЕСТР ПО НОВЫМ СТАНДАРТАМ ОСНАЩЕНИЯ</p>`,
    },
    {
      id: 2,
      title: 'АККРЕДИТАЦИЯ',
      description: `<p>ЗАРЕГИСТРИРУЕМ СПЕЦИАЛИСТА В ФРМР И НА ПОРТАЛЕ НМФО</p> <br> <p>СДЕЛАЕМ ПОЛНЫЙ АУДИТ ВАШИХ ДОКУМЕНТОВ</p> <br> <p>ПОМОГАЕМ СОБРАТЬ ПОРТФОЛИО, НАПИСАТЬ ОТЧЕТ И ОТПРАВИТЬ В АККРЕДИТАЦИОННЫЙ ЦЕНТР</p>`,
    },
    {
      id: 3,
      title: 'ЛИЦЕНЗИРОВАНИЕ',
      description: `<p>ПОДТОГОВИМ ПАКЕТ ДОКУМЕНТОВ</p> <br> <p>МЫ ЗАРЕГИСТРИРУЕМ ВАС В ЕГИСЗ В КРАТЧАЙШИЕ СРОКИ</p> <br> <p>СДАДИМ ВАШ ПАКЕТ ДОКУМЕНТОВ В ЛИЦЕНЗИОННЫЙ ОТДЕЛ</p>`,
    },
    {
      id: 4,
      title: 'САЙТЫ',
      description: `<p>РАЗРАБОТАЕМ САЙТ СОГЛАСНО ТРЕБОВАНИЯМ НПА</p> <br> <p>ПОДГОТОВИМ НЕОБХОДИМЫЕ ДОКУМЕНТЫ ДЛЯ ИХ РАЗМЕЩЕНИЯ НА САЙТ</p> <br> <p>ПРЕДОСТАВИМ ВАМ ПОЛНЫЙ КОМПЛЕКС ПО ЕЖЕМЕСЯЧНОЙ ПОДДЕРЖКЕ И РАЗВИТИЮ ВАШЕГО ПРОЕКТА</p>`,
    },
  ]
  public contactForm: FormGroup;
  public tableWidth = window.innerWidth;

  get success$(): Observable<boolean> {
    return this.ms.success$.asObservable();
  }

  get error$(): Observable<boolean> {
    return this.ms.error$.asObservable();
  }

  constructor(private fb: FormBuilder, private ms: MailService) {

    // for more details on config options please visit fullPage.js docs
    this.config = {

      // fullpage options
      licenseKey: 'YOUR LICENSE KEY HERE',
      anchors: ['first', 'second', 'third', 'fourth', 'fifth'],
      navigation: true,
      menu: '#menu',
      navigationPosition: 'right',
      onLeave: (origin: any, destination: any, direction: any, trigger: any) => {
        this.isVisible = false;
      },
      afterLoad: (origin: any, destination: any, direction: any) => {
        if (this.timer) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => this.isVisible = true, 0)
      },
    };
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      message: ['', Validators.required],
      isAgree: ['', Validators.requiredTrue],
    })
  }

  ngOnInit(): void {
    if (this.tableWidth < 1000) {
      this.infoIndex = 1;
    }
  }

  getRef(fullPageRef: any) {
    this.fullpage_api = fullPageRef;
  }

  public showDescription(i: number): void {
    this.infoIndex = i + 1;
  }

  public hideDescription() {
    this.infoIndex = 0;
  }

  public send() {
    if (this.contactForm.valid) {
      this.ms.sendFormToEmail(this.contactForm.value);
      this.contactForm.reset();
    }
  }
}
