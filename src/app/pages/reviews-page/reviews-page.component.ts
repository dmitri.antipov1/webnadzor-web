import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ReviewsService } from '../../services/reviews.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fade, inOutPaneAnimation } from '../../animations/animations';

@Component({
  selector: 'app-reviews-page',
  templateUrl: './reviews-page.component.html',
  styleUrls: ['./reviews-page.component.css'],
  animations: [
    inOutPaneAnimation,
    fade
  ],
})
export class ReviewsPageComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef | undefined;
  public isFormVisible: boolean = false;
  private timer: any;
  public enabledReviews$ = this.rs.availableReviews$;
  public reviewForm: FormGroup;
  public isPopup$ = this.rs.backMessage$;

  constructor(
    private readonly fb: FormBuilder,
    private readonly rs: ReviewsService
  ) {
    this.reviewForm = this.fb.group({
      author: ['', Validators.required],
      organization: ['', Validators.required],
      review: ['', [Validators.required, Validators.maxLength(6800)]],
      isAgreements: [false, Validators.requiredTrue],
    })
  }

  ngOnInit(): void {
    this.rs.getAvailableReviews();
    document.body.style.backgroundImage = 'url(assets/feedback-bg.png)';
  }

  toggleFormState(): void {
    this.isFormVisible = true;
    if(this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      if(this.myScrollContainer?.nativeElement) {
        this.myScrollContainer?.nativeElement.scrollIntoView({ behavior: 'smooth'});
      }
    }, 500)
  }

  public sendReview(): void {
    if (this.reviewForm.valid) {
      this.rs.addReview(this.reviewForm.value);
      this.reviewForm.reset();
    }
  }

  public closePopup() {
    this.rs.backMessage$.next(null);
  }
}
